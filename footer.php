<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package izara
 */

?>

	</div><!-- #content -->
	<footer id="colophon" class="izara-site-footer">
		<?php if( is_active_sidebar('footer-widgets')) : ?>
		<div class="footer-top-widgets">
			<div class="container">
				<div class="row">
					<?php dynamic_sidebar('footer-widgets'); ?>
				</div>
			</div>
		</div>
	    <?php endif; ?>
		
		<div class="izara-footer-area">
			<div class="container">
				<div class="row">
					<div class="col-md-12 text-center">
					 &copy; <?php esc_html_e( 'Copyright by ', 'izara' ); ?> <a target="_balnk" href="<?php echo esc_url('http://codepopular.com'); ?>"><?php esc_html_e( 'CodePopular', 'izara' ); ?></a> <?php echo esc_html( date_i18n( __( 'Y ', 'izara' ) ) ); ?>
					</div>
				</div>
			</div>
		</div>
	</footer><!-- #colophon -->
</div><!-- #page -->

<div class="back-to-top"><i class="fa fa-angle-up"></i></div>
</div><!-- #page -->
<?php wp_footer(); ?>
</body>
</html>
