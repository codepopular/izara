== izara==
Contributors: codepopular
Tags:  blog, right-sidebar, custom-background, featured-images
Requires at least: 4.4
Tested up to: 5.4
Version: 1.0.0
License: GNU General Public License v2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Free Blog WordPress Theme

== Description ==
izara is the soluation to make blog, consultance and business website. with izara you can use any page builder like elementor, visual composer or as you want. it its full responsive theme and all browser supported. so lets's enjoy it. 


== Installation ==

1. After login wordpress go to Appearance > Themes and click the Add New button.
2. Click Upload and Choose File, then select the theme's .zip file. Click Install Now.
3. Click Activate to use your new theme right away.

= Theme Features Usage =
All available options can be used from Appearance->Customize


== RESOURCES ==
= Twitter Bootstrap =
License: MIT License 
Source - http://getbootstrap.com 

=glyphicons=
Code licensed MIT, docs CC BY 3.0.
Source - https://getbootstrap.com/docs/3.3/components/

= FontAwesome Icons =
License: SIL Open Font License, 1.1,
 https://opensource.org/licenses/OFL-1.1
Source: https://www.fontawesome.io
Code licensed under MIT License



All other resources and theme elements are licensed under the [GNU GPL](http://www.gnu.org/licenses/gpl-2.0.txt), version 2 or later.


= Version 1.0.0 =
* Initial release